-- =============================================================================
--
-- Vile widget for Awesome 4.x (~/.config/awesome/vile/widgets/audio.lua)
--
-- @see
-- @author Caroline Åling
--
-- =============================================================================

local beautiful = require("beautiful")
local spawn = require("vile.helpers.spawn")
local wibox = require("wibox")

--
local widget = wibox.widget {
	{
		id = "myprogressbar",
		max_value = 100,
		min_value = 0,
		value = 0,
		widget = wibox.widget.progressbar
	},
	{
		align = "center",
		id = "mytextbox",
		text = "N/A",
		widget = wibox.widget.textbox
	},
	forced_width = 20,
	layout = wibox.layout.stack,
	set_output = function(self, data)
		self.mytextbox.text = data.muted and "Muted" or data.level.."%"
		self.myprogressbar.value = data.muted and 0 or data.level
	end
}

--
widget:connect_signal("button::press", function(_, _, _, button)
	if button == 1 then spawn("pactl set-sink-mute 3 toggle")
	elseif button == 4 then spawn("pactl set-sink-volume 3 +3%")
	elseif button == 5 then spawn("pactl set-sink-volume 3 -3%")
	end
end)

--
--widget.connect_signal("mouse::enter", function()

--end)

--
--widget.connect_signal("mouse::leave", function()

--end)

return widget
