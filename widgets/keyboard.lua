-- =============================================================================
--
-- Vile widget for Awesome 4.x (~/.config/awesome/vile/widgets/keyboard.lua)
--
-- @see
-- @author Caroline Åling
--
-- =============================================================================

local spawn = require("vile.helpers.spawn")
local wibox = require("wibox")

--local layout = spawn("setxkbmap -query"):match("layout:%s+(%l+)")
--local image = string.format("/mnt/data/code/etc/img/flags/%s.png", layout)

function get_image()
	return string.format(
		"/mnt/data/code/etc/img/flags/%s.png",
		spawn("setxkbmap -query"):match("layout:%s+(%l+)")
	)
end

local widget = wibox.widget {
	image = get_image(),
	resize = false,
	widget = wibox.widget.imagebox
}

--awesome.connect_signal("xkb::group_changed", function()
--	widget:set_image(get_image())
--end)

awesome.connect_signal("xkb::map_changed", function()
	widget:set_image(get_image())
end)

return widget
