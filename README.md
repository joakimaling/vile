# Vile

> Widgets for Awesome

[![Licence][licence-badge]][licence-url]

This repository contains components written for the Awesome window manager, with
lots of inspiration coming from similar projects such as [Vicious][vicious-url],
[Lain][lain-url] & [streetturtle][turtle-url]'s widgets customised according to
my own preferences.

## Installation

:bulb: Requires [Awesome](https://awesomewm.org/).

First install Awesome using your favourite package manager, then clone this
repository:

```sh
git clone https://gitlab.com/joakimaling/vile.git ~/.config/awesome/vile
```

## Usage

First, in your `~/.config/awesome/rc.lua` add:

```lua
local vile = require("vile")
```
```lua
vile.register(worker, pattern, timeout, arguments)
```
Argument    | Type             | Notes
----------- | ---------------- | -----------------------------------------------
`worker`    | function         | [Vile worker][v] or [custom worker function][c]
`pattern`   | function, string | Formatter pattern for the worker's return data
`timeout`   | number           | Interval, in seconds, for the timeout event
`arguments` | string, table    | Arguments for the worker

### Custom worker function

You can construct your own widgets by following below template and then register
it with the above Vile function. The worker function's only argument is optional
but it must return a table with data.

```lua
local spawn = require("vile.helpers.spawn")

function worker(args)
	local args = args or {}

	local data = spawn("ls | wc -l")
	local files = data:match("(%d+)")

	return {
		number_of_files = tonumber(files)
	}
end
```

## Licence

Released under MIT. See [LICENSE][licence-url] for more.

Coded with :heart: by [carolinealing][user-url].

[lain-url]: https://gitlab.com/lcpz/lain
[licence-badge]: https://img.shields.io/gitlab/license/carolinealing/vile.svg
[licence-url]: LICENSE
[turtle-url]: https://gitlab.com/streetturtle/awesome-wm-widgets
[user-url]: https://gitlab.com/carolinealing
[vicious-url]: https://gitlab.com/vicious-widgets/vicious
[v]: https://gitlab.com/carolinealing/vile/tree/master/widgets
[c]: https://gitlab.com/carolinealing/vile#customworkerfunction
