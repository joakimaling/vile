-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/mpd.lua)
--
-- Provides information about current song played by MPD.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#mpdlua
-- @author Caroline Åling
--
-- Requires: mpc
--
-- =============================================================================
local spawn = require("vile.helpers.spawn")
local split = require("vile.helpers.split")

-- Returns the full path to the cover art for the given song.
--
-- @param filename full path to the current song being played
-- @param pattern  valid image file extensions as a regex pattern
--
-- @return full path to the cover art
--
local function get_cover_art(filename, pattern)
	return spawn(("find '%s' -maxdepth 1 -type f | egrep -i -m 1 '%s'"):format(
		filename:match(".*/"),
		pattern
	))
end

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local host = args.host or os.getenv("MPD_HOST") or "127.0.0.1"
	local library = args.library or ("%s/Music"):format(os.getenv("HOME"))
	local pattern = args.pattern or "*\\.(gif|jpe?g|png)$"
	local port = args.port or os.getenv("MPD_PORT") or 6600
	local step = args.step or 3

	-- Format string to extract song details from MPC. The separator is unique &
	-- mustn't conflict with characters in the name of the album or the title
	local fmt = "'%album%§%artist%§%date%§%file%§%title%'"

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn(("mpc -f %s -h %s -p %d status"):format(fmt, host, port))

	-- Extracts only the essential values from the result
	local song, status, progress, volume, mode_repeat, mode_random = data:match(
		"(.+)%c%[(%l+)%].+%((%d+)%%%).-volume: ([^%%]+).+repeat: (%l+).+random: (%l+)"
	)

	-- Splits the song string into its constituent parts
	song = split(song, "§")

	return {
		album = song[1],
		artist = song[2],
		cover_art = get_cover_art(("%s/%s"):format(library, song[4]), pattern),
		mode_random = mode_random == "on",
		mode_repeat = mode_repeat == "on",
		progress = tonumber(progress),
		status = status,
		title = song[5],
		volume = volume == "n/a" and 0 or tonumber(volume),
		year = tonumber(song[3])
	}
end

-- Launch the player.
--
--function mpd.launch()
--	spawn(("ncmpcpp -h %s -p %d"):format(host, port))
--end

-- Skips to the previous track.
--
--function mpd.previous()
--	spawn(("mpc -h %s -p %d prev").format(host, port))
--end

-- Skips to the next track.
--
--function mpd.next()
--	spawn(("mpc -h %s -p %d next").format(host, port))
--end

-- Decreases the volume.
--
--function mpd.lower()
--	spawn(("mpc -h %s -p %d volume -%d%%").format(host, port, step))
--end

-- Increases the volume.
--
--function mpd.raise()
--	spawn(("mpc -h %s -p %d volume +%d%%").format(host, port, step))
--end

-- Toggles between play/pause.
--
--function mpd.toggle()
--	spawn(("mpc -h %s -p %d toggle").format(host, port))
--end

return worker
