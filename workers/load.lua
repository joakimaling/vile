-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/load.lua)
--
-- Provides system load during one, five & fifteen minutes.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#loadlua
-- @author Caroline Åling
--
-- =============================================================================
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @return Data for use in widgets
--
local function worker()
	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn("cat /proc/loadavg")

	-- Extracts only the essential values from the result
	local last1, last5, last15 = data:match("(%d+%.%d+) (%d+%.%d+) (%d+%.%d+)")

	return {
		last1 = tonumber(last1),
		last5 = tonumber(last5),
		last15 = tonumber(last15)
	}
end

return worker
