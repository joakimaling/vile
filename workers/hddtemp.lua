-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/hddtemp.lua)
--
-- Provides hard drive temperatures using the hddtemp daemon.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#hddtemplua
-- @author Caroline Åling
--
-- Requires: curl, hddtemp
--
-- =============================================================================
local curl = require("vile.helpers.curl")
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local port = args.port or 7634
	local output = {}

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn(curl("http://127.0.0.1:%d"):format(port))

	-- Extracts only the essential values from the result
	for disk, name, temp in data:gmatch("|(.-)|(.-)|(%d+)|[CF]|") do
		output[disk] = {
			name = name,
			temp = tonumber(temp)
		}
	end

	return output
end

return worker
