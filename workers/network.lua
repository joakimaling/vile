-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/network.lua)
--
-- Provides information about network traffic.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#networklua
-- @author Caroline Åling
--
-- =============================================================================
local round = require("vile.helpers.round")
local spawn = require("vile.helpers.spawn")

-- Old values used to calculate the difference
local old_time, old_rx, old_tx = 0, 0, 0

-- Tries to fetch the network device name.
--
-- @return Device name
--
local function get_device()
	return spawn("ip link | grep -iv 'LOOPBACK'"):match("(%w+): <")
end

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local device = args.device or get_device() or "eth0"

	-- Runs a command which gathers the raw data based on given arguments
	local rx = spawn(("cat /sys/class/net/%s/statistics/rx_bytes"):format(device))
	local tx = spawn(("cat /sys/class/net/%s/statistics/tx_bytes"):format(device))
	local carrier = spawn(("cat /sys/class/net/%s/carrier"):format(device))
	local status = spawn(("cat /sys/class/net/%s/operstate"):format(device))

	local time = os.time()
	local interval = time - old_time

	if interval <= 0 then interval = 1 end

	-- Calculate the difference
	local received = (rx - old_rx) / interval
	local transmitted = (tx - old_tx) / interval

	old_time = time
	old_rx = rx
	old_tx = tx

	return {
		carrier = carrier ~= "0",
		rx = round(received),
		status = status,
		total_rx = tonumber(rx),
		total_tx = tonumber(tx),
		tx = round(transmitted)
	}
end

return worker
