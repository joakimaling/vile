-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/drives_io.lua)
--
-- Provides disk I/O statistics.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#drives_iolua
-- @author Caroline Åling
--
-- =============================================================================
local join = require("vile.helpers.join")
local spawn = require("vile.helpers.spawn")

-- Old values used to calculate the difference
local old_time, old_read, old_write = 0, {}, {}

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local disks = args.disks or {"sda1"}
	local output = {}

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn(("grep '%s' /proc/diskstats"):format(join(disks, "\\|")))

	-- Extracts only the essential values from the result
	for device, read, write in
		data:gmatch("(%w+) %d+ %d+ (%d+) %d+ %d+ %d+ (%d+)") do
		local time = os.time()
		local interval = time - old_time

		if interval <= 0 then interval = 1 end

		-- Calculate the difference & add it to the table
		output[device] = {
			read = (read - (old_read[device] or 0)) / interval,
			write = (write - (old_write[device] or 0)) / interval
		}

		old_time = time
		old_read[device] = read
		old_write[device] = write
	end

	return output
end

return worker
