-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/drives.lua)
--
-- Provides information about given partitions.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#driveslua
-- @author Caroline Åling
--
-- =============================================================================
local join = require("vile.helpers.spawn")
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local drives = args.drives or {"/dev/sda1"}
	local output = {}

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn(("df -PTk %s"):format(join(drives)))

	-- Extracts only the essential values from the result
	for device, file_system, size, used, free, percent, mount_point in
		data:gmatch("(.-)%s+(%w+)%s+(%d+)%s+(%d+)%s+(%d+)%s+(%d+)%%%s+(.-)%c") do

		output[device] = {
			device = device,
			file_system = file_system,
			free = tonumber(free),
			mount_point = mount_point,
			percent = tonumber(percent),
			size = tonumber(size),
			used = tonumber(used)
		}
	end

	return output
end

return worker
