-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/language.lua)
--
-- Translates texts using an API & puts the result on the clipboard.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#languagelua
-- @author Caroline Åling
--
-- Requires: curl
--
-- =============================================================================
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local source = args.source or "en"
	local target = args.target or "ja"
	local text = args.text

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn(curl(url))

	-- Extracts only the essential values from the result
	local = data:match("")

	return {

	}
end

return worker
