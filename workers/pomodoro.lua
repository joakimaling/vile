-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/pomodoro.lua)
--
-- .
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#pomodorolua
-- @author Caroline Åling
--
-- Requires: pomo
--
-- =============================================================================
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn("pomo status")

	-- Extracts only the essential values from the result
	local minutes, seconds = data:match("(%-)(%d+)s")

	return {
		remaining = {
			hours =
			minutes =
		},
		pomodoros = {
			current =
			total =
		}
		paused =
	}
end

return worker
