-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/updates.lua)
--
-- Provides information about available system updates.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#updateslua
-- @author Caroline Åling
--
-- Requires: aptitude | pacman
--
-- =============================================================================
local spawn = require("vile.helpers.spawn")

-- Tries to fetch the distribution ID.
--
-- @return Distribution ID
--
local function get_id()
	return spawn("cat /etc/os-release"):match("ID=(%l+)")
end

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local distro = args.distro or get_id() or "arch"

	-- A map between distribution IDs and package managers. This allows the
	-- widget to use the correct one for the current distribution
	local managers = {
		["arch"] = "pacman -Qu",
		["Arch C"] = "checkupdates",
		["Arch S"] = "yes | pacman -Sup",
		["Debian"] = "apt-show-versions -b -u",
		["Fedora"] = "yum list updates",
		["FreeBSD"] = "pkg version -I -l '<'",
		["Mandriva"] = "urpmq --auto-select",
		["ubuntu"] = "aptitude search '~U'"
	}

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn(managers[distro])

	-- Extracts only the essential values from the result
	local _, count = data:gsub("\n", "\n")

	return {
		list = data:gsub("\n$", ""),
		total = count
	}
end

return worker
