-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/hwmon.lua)
--
-- Provides temperature information using the hardware monitor.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#hwmonlua
-- @author Caroline Åling
--
-- =============================================================================
local round = require("vile.helpers.round")
local spawn = require("vile.helpers.spawn")

-- Returns the path to the sensor of which name file's contents matches the
-- given name.
--
-- @param name Name of the sensor
-- @raturn Path to the sensor
--
local function get_path(name)
	local path = "/sys/class/hwmon"
	local data = spawn(("ls -1 %s"):format(path))

	-- Search each folder's name file which contents that matches the given name
	for x in data:gmatch("(%w+)") do
		if name == spawn(("cat %s/%s/name"):format(path, x)):match("(%w+)") then
			return ("%s/%s"):format(path, x)
		end
	end

	return nil
end

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local name = args.name or "coretemp"
	local output = {}

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn(("cat %s/temp*_input"):format(get_path(name)))

	-- Extracts only the essential values from the result
	for temperature in data:gmatch("(%-?%d+)") do
		table.insert(output, round(temperature / 1000))
	end

	return output
end

return worker
