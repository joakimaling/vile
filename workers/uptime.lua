-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/uptime.lua)
--
-- Provides current system uptime.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#uptimelua
-- @author Caroline Åling
--
-- =============================================================================
local spawn = require("vile.helpers.spawn")

-- Splits a Unix time value into its parts of days, hours, minutes & seconds.
--
-- @param number Seconds since boot time
-- @return Table containging days, hours, minutes & seconds since boot time
--
local function get_time(value)
	return {
		days = math.floor(value / 86400),
		hours = math.floor((value / 3600) % 24),
		minutes = math.floor((value / 60) % 60),
		seconds = math.floor(value % 60)
	}
end

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @return Data for use in widgets
--
local function worker()
	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn("cat /proc/uptime")

	-- Extracts only the essential values from the result
	local total, idle = data:match("(%d+%.?%d*) (%d+%.?%d*)")

	return {
		idle = get_time(idle),
		total =	get_time(total)
	}
end

return worker
