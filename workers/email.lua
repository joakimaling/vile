-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/imap.lua)
--
-- .
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#imaplua
-- @author Caroline Åling
--
-- Requires: curl
--
-- =============================================================================
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local email = args.email
	local password = args.password
	local port = args.port or 993
	local server = args.server

	-- Runs a command which gathers the raw data based on given arguments
	local command = "curl -X 'STATUS INBOX (MESSAGES RECENT UNSEEN)' -u '%s:%s' imaps://%s:%d/INBOX"
	local data = spawn(command:format(email, password, server, port))

	-- Extracts only the essential values from the result
	local = data:match("")

	return {

	}
end

return worker
