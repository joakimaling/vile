-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/ram.lua)
--
-- Provides current RAM & swap utilisation.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#ramlua
-- @author Caroline Åling
--
-- =============================================================================
local round = require("vile.helpers.round")
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @return Data for use in widgets
--
local function worker()
	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn("awk '/(Mem|wap)(F|T)|Bu|^Ca/ {print $2}' /proc/meminfo")

	-- Extracts only the essential values from the result
	local total, free, buffer, cache, swap_total, swap_free = data:match(
		"(%d+).-(%d+).-(%d+).-(%d+).-(%d+).-(%d+)"
	)

	return {
		memory = round(100 * (total - free - buffer - cache) / total),
		swap = round(100 * (swap_total - swap_free) / swap_total)
	}
end

return worker
