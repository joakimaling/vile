-- =============================================================================
--
-- Vile workers for Awesome 4.x (~/.config/awesome/vile/workers/init.lua)
--
-- @see https://gitlab.com/carolinealing/vile
-- @author Caroline Åling
--
-- =============================================================================

return setmetatable({_NAME = "vile.workers"}, {__index = function(table, key)
	return rawget(table, key) or require(("%s.%s"):format(table._NAME, key))
end})
