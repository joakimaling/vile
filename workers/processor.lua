-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/processor.lua)
--
-- Provides current CPU utilisation.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#processorlua
-- @author Caroline Åling
--
-- =============================================================================
local round = require("vile.helpers.round")
local spawn = require("vile.helpers.spawn")

-- Old values used to calculate the difference
local old_idle, old_total = {}, {}

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @return Data for use in widgets
--
local function worker()
	local cores, index = {}, 0

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn("grep '^cpu' /proc/stat")

	-- Extracts only the essential values from the result
	for user, nice, system, idle, iowait, irq, softirq in
		data:gmatch("cpu%d?.-(%d+) (%d+) (%d+) (%d+) (%d+) (%d+) (%d+)") do
		local total = user + nice + system + idle + iowait + irq + softirq
		local idle = idle + iowait

		-- Calculate the difference
		local total_diff = total - (old_total[index] or 0)
		local idle_diff = idle - (old_idle[index] or 0)

		-- Calculate the percentage & add it to the table
		table.insert(cores, round(100 * (total_diff - idle_diff) / total_diff))

		old_total[index] = total
		old_idle[index] = idle
		index = index + 1
	end

	return {
		all = table.remove(cores),
		cores = cores
	}
end

return worker
