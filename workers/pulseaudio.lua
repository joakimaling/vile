-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/pulseaudio.lua)
--
-- Provides volume & mute status of a sound device using PulseAudio.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#pulseaudiolua
-- @author Caroline Åling
--
-- Requires: pacmd
--
-- =============================================================================
local escape = require("vile.helpers.escape")
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local sink = args.sink

	-- Use the name, if given a sink, otherwise use the default
	local value = "* index:"
	if sink ~= nil then value = ("name: <%s>"):format(escape(sink)) end

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn("pacmd list-sinks")

	-- Extracts only the essential values from the result
	local level, muted = data:match(value .. ".-volume:.-(%d+)%%.*muted: (%l+)")

	return {
		level = tonumber(level),
		muted = muted ~= "no"
	}
end

return worker
