-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/backlight.lua)
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#backlightlua
-- @author Caroline Åling
--
-- Requires: xbacklight
--
-- =============================================================================
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker()

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn("xbacklight -get") -- "light -G"
	-- local INC_BRIGHTNESS_CMD = "light -A 5" -- "xbacklight -inc 5"
	-- local DEC_BRIGHTNESS_CMD = "light -U 5" -- "xbacklight -dec 5"

	return {
		level = tonumber(data:match("(%d+)%%")) / 100
	}
end

return worker
