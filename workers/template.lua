-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/.lua)
--
-- Provides  using .
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#lua
-- @author Caroline Åling
--
-- Requires:
--
-- =============================================================================
local round = require("vile.helpers.round")
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}

	-- Error handling; missing arguments
	if  == nil then
		return {
			error = ".lua - Missing arguments"
		}
	end

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn((""):format())

	-- Error handling; command failed
	if  == nil then
		return {
			error = ".lua - Command failed"
		}
	end

	-- Extracts only the essential values from the result
	local = data:match("")

	-- Error handling; extraction failed
	if  == nil then
		return {
			error = ".lua - Extraction failed"
		}
	end

	return {

	}
end

return setmetatable({}, {__call = function(_, ...) return worker(...) end})
