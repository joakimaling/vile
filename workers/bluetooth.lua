-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/bluetooth.lua)
--
-- Provides bluetooth status & enable/disable functionality using Bluez.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#bluetoothlua
-- @author Caroline Åling
--
-- Requires: buetoothctl
--
-- =============================================================================
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @return Data for use in widgets
--
local function worker()
	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn("bluetoothctl show")

	-- Extracts only the essential values from the result
	local mac_address, powered = data:match("Controller (.+) %(.*Powered: (%l+)")

	return {
		mac_address = mac_address,
		powered = powered ~= "no"
	}
end

return worker
