# Vile workers

Here's a list of available workers (the core of widgets) in Vile and information
on how to use them. The tables show possible arguments and return data.

All workers are stand-alone and can be used for testing in a terminal using the
Lua interpreter. Assuming this project is located in `~/.config/awesome/vile`

```
cd ~/.config/awesome
lua -l vile
Lua 5.3.5  Copyright (C) 1994-2018 Lua.org, PUC-Rio
> ram = vile.workers.ram()
> print(ram.memory, ram.swap)
134	0
```

## alsa.lua

Provides volume & mute status of a sound device using ALSA. Returns level of
volume and mute status.

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`card`    | number | `0`           | Number of the sound card, if you have many
`channel` | string | `"Master"`    | Name of the channel
`step`    | number | `3`           | Amount in percent to raise/lower the volume

## bat.lua

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`ac`      | string | `"AC0"`       |
`battery` | table  | `{"BAT0"}`    |

## bright.lua

## cpu.lua

Provides the current CPU utilisation in percent. Requires **no arguments** and
returns a table containing values for all cores.

key       |  type  | example       | notes
--------- | :----: | ------------- | -------------------------------------------
`all`     | number | `16`          | Total value for all cores
`cores`   | table  | `{25, 33}`    | Values for all cores separately

## dio.lua

Provides disk I/O statistics. Requires **one argument** and returns a table
containing details about each requested partition.

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`disks`   | table  | `{"sda1"}`    |

## fs.lua

Provides information about the space of a partition. Requires **one argument**
and returns a table containing details about each requested partition.

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`disks`   | table  | `{"sda1"}`    |

## hddtemp.lua

Provides hard drive temperatures using the hddtemp daemon. Returns a table with
the device path as keys and the name and temperature as values.

:bulb: How to setup [the daemon](https://wiki.archlinux.org/index.php/Hddtemp).

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`port`    | number | `7634`        | Port number to which the daemon listens

## hwmon.lua

Provides temperature information using the hardware monitor. Returns a table
all the temperatures connected to the given sensor.

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`name`    | string |`"coretemp"`   | Name of the sensor to get temperatures from

## imap.lua

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`email`   | string |               |
`password`| string |               |
`port`    | number | `993`         | Port number to which the server listens
`server`  | string |               |

## lang.lua

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`source`  | string | `en`          | Two-letter code for the source language
`target`  | string | `ja`          | Two-letter code for the target language

## load.lua

Provides the average system load during one, five & fifteen minutes. Requires
**no arguments** and returns the three values as they are.

key       |  type  | example       | notes
--------- | :----: | ------------- | -------------------------------------------
`last1`   | number | `0.43`        | Average load during one minute
`last5`   | number | `0.5`         | Average load during five minutes
`last15`  | number | `0.52`        | Average load during fifteen minutes

## mpd.lua

Provides information about current song played by MPD.

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`host`    | string | `"127.0.0.1"` | IP-address of the server hosting the daemon
`port`    | number | `6600`        | Port number to which the daemon listens

key       | type   | example       | notes
--------- | :----: | ------------- | -------------------------------------------
`album`   | string | `LEVEL3`      | Album title
`artist`  | string | `Perfume`     | Name of artist
`cover`   | string | `...`         | Path to the album cover, if it exists
`progress`| number | `20`          | Percentage of the track has been played
`random`  | boolean| `false`       | If random mode is active
`repeat`  | boolean| `true`        | If repeat mode is active
`status`  | string | `playing`     | One of *paused*, *playing* or *stopped*
`title`   | string | `Clockwork`   | Song title
`volume`  | number | `38`          | Volume of the stream
`year`    | number | `2011`        | Release year

## net.lua

Provides information about network traffic. Requires **one argument** and
returns a table of information about the network traffic.

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`device`  | string | `"eth0"`      | The network device to be monitored

key       |  type  | example       | notes
--------- | :----: | ------------- | -------------------------------------------
`carrier` | boolean| `true`        | If the device is currently handling traffic
`rx`      | number | `0`           | Current rate of received data
`status`  | string | `"up"`        | Link status
`total_rx`| number | `79797406`    | Total amount of received data
`total_tx`| number | `49172764`    | Total amount of transmitted data
`tx`      | number | `37`          | Current rate of transmitted data

## owm.lua

Provides current weather information in a given city. Requires **two arguments**
and returns a table of weather data as seen below.

:bulb: Requires a private API identifier given by [OpenWeatherMap.org][owm-url].

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`appid`   | string |               | **Required** Unique hexadecimal identifier
`city`    | number |               | **Required** City number, at OpenWeatherMap
`lang`    | string | `"en"`        | A two-letter language code
`unit`    | string | `"metric"`    | One of *default*, *imperial* or *metric*

key       |  type  | example       | notes
--------- | :----: | ------------- | -------------------------------------------
`details` | string | `"fog"`       | Detailed description of the weather type
`humidity`| number | `92`          | Air humidity in percent
`icon`    | string | `"50n"`       | Alpha-numeric string representing an icon
`main`    | string | `"Fog"`       | Weather type
`pressure`| number | `990`         | Air pressure
`sunrise` | number | `1552540300`  | Time of sunrise (Unix time value)
`sunset`  | number | `1552582512`  | Time of sunset (Unix time value)
`temp`    | table  | `{1, 2, -3}`  | Temperatures; *current*, *day* and *night*
`wind`    | table  | `{"W", 1.06}` | Wind; *directions* and *speed*

## pkg.lua

Provides information about available system updates. Has **one argument**, which
is optional. Unprovided the widget will try to get it from `/etc/os-release` and
if unsuccessful the widget will fail. Returns a value representing the number of
updates available and a list of them.

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`distro`  | string | `arch`        | The identifier of the distribution

key       |  type  | example       | notes
--------- | :----: | ------------- | -------------------------------------------
`list`    | string | `...`         | List of updates (*distribution dependent*)
`total`   | number | `42`          | Number of available updates

## pomo.lua

## pop.lua

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`email`   | string |               |
`password`| string |               |
`port`    | number | `995`         | Port number to which the server listens
`server`  | string |               |

## pulse.lua

Provides volume & mute status of a sound device using PulseAudio. Returns level
of volume and mute status.

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`sink`    | string |               | Name of sink. Uses default if not provided
`step`    | number | `3`           |

## raid.lua

## ram.lua

Provides the current RAM and swap utilisation. Requires **no arguments** and
returns the percentage for both RAM and swap.

key       |  type  | example       | notes
--------- | :----: | ------------- | -------------------------------------------
`memory`  | number | `33`          | Percent of RAM being used
`swap`    | number | `0`           | Percent of swap being used

## task.lua

## temp.lua

## uptime.lua

Provides the current system uptime. Requires **no arguments** and returns the
values stored as tables of days, hours, minutes and seconds since boot time.

key       |  type  | example       | notes
--------- | :----: | ------------- | -------------------------------------------
`idle`    | table  |               | Days, hours, minutes and seconds being idle
`total`   | table  |               | Days, hours, minutes and seconds since boot

## wifi.lua

argument  |  type  | default       | notes
--------- | :----: | ------------- | -------------------------------------------
`device`  | string | `"wlan0"`     |

[owm-url]: https://openweathermap.org/
