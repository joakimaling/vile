-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/battery.lua)
--
-- Provides information about the system battery such as charge and status.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#batterylua
-- @author Caroline Åling
--
-- =============================================================================
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local unit = args.unit or "BAT0"

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn(("cat /sys/class/power_supply/%s/uevent"):format(unit))

	-- Extracts only the essential values from the result
	local status, present, full, now = data:match(
		".-STATUS=(%a+).-PRESENT=(%d).-FULL=(%d+).-NOW=(%d+)"
	)

	return {
		charging = status ~= "Discharging",
		percent = 100 * (now / full),
		present = present ~= 0
	}
end

return worker
