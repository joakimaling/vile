-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/owm.lua)
--
-- Provides current weather information in a given city.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#owmlua
-- @author Caroline Åling
--
-- Requires: curl
--
-- =============================================================================
local curl = require("vile.helpers.curl")
local round = require("vile.helpers.round")
local spawn = require("vile.helpers.spawn")

-- Directions for converting from degrees
local wind_directions = {"N", "NE", "E", "SE", "S", "SW", "W", "NW"}

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local appid = args.appid
	local city = args.city
	local lang = args.lang or "en"
	local unit = args.unit or "metric"

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn(curl("http://api.openweathermap.org/data/2.5/weather?appid=%s&id=%d&lang=%s&units=%s"):format(appid, city, lang, unit))

	-- Extracts only the essential values from the result
	local main, details, icon, current, night, day, pressure, humidity, speed,
		degrees, sunrise, sunset = data:match(
		'"main":"(%a+)","description":"(.-)","icon":"(%w+)".-"temp":' ..
		'(%-?%d+%.?%d*).-temp_min":(%-?%d+%.?%d*),"temp_max":(%-?%d+%.?%d*),' ..
		'"pressure":(%d+),"humidity":(%d+).-"speed":(%d+%.?%d*),"deg":(%d+)' ..
		'.-"sunrise":(%d+),"sunset":(%d+)'
	)

	return {
		details = details,
		humidity = tonumber(humidity),
		icon = icon,
		main = main,
		pressure = tonumber(pressure),
		sunrise = tonumber(sunrise),
		sunset = tonumber(sunset),
		temp = {
			current = tonumber(current),
			day = tonumber(day),
			night = tonumber(night),
		},
		wind = {
			 direction = wind_directions[1 + round(degrees / 45)],
			 speed = tonumber(speed)
		}
	}
end

return worker
