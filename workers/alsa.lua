-- =============================================================================
--
-- Vile worker for Awesome 4.x (~/.config/awesome/vile/workers/alsa.lua)
--
-- Provides volume & mute status of a sound device using ALSA.
--
-- @see https://gitlab.com/carolinealing/vile/tree/master/workers#alsalua
-- @author Caroline Åling
--
-- Requires: amixer
--
-- =============================================================================
local spawn = require("vile.helpers.spawn")

-- The core of the widget. It gathers the raw data, does the calculations, based
-- on the given arguments, if any, & returns it as a table.
--
-- @param args Arguments for the worker
-- @return Data for use in widgets
--
local function worker(args)
	local args = args or {}
	local card = args.card or 0
	local channel = args.channel or "Master"

	-- Runs a command which gathers the raw data based on given arguments
	local data = spawn(("amixer -c %d get %s"):format(card, channel))

	-- Extracts only the essential values from the result
	local level, status = data:match("(%d+)%%.*%[(%l+)")

	return {
		level = tonumber(level),
		muted = status ~= "on"
	}
end

return worker
