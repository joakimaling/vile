-- =============================================================================
--
-- Vile helper for Awesome 4.x (~/.config/awesome/vile/helpers/join.lua)
--
-- Joins a table with given delimiter and returns a string.
--
-- @author Caroline Åling
--
-- =============================================================================

return function(array, delimiter)
	delimiter = delimiter or " "
	local result = array[1]

	for i = 2, #array do
		result = ("%s%s%s"):format(result, delimiter, array[i])
	end

	return result
end
