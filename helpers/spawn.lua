-- =============================================================================
--
-- Vile helper for Awesome 4.x (~/.config/awesome/vile/helpers/spawn.lua)
--
-- Runs a command and returns its output.
--
-- @author Caroline Åling
--
-- =============================================================================

return function(command)
	local pipe = io.popen(("/usr/bin/%s 2>/dev/null"):format(command))
	local data = pipe:read("*a")
	pipe:close()

	return data
end
