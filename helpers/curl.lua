-- =============================================================================
--
-- Vile helper for Awesome 4.x (~/.config/awesome/vile/helpers/curl.lua)
--
-- Creates a standard call to given address using cURL.
--
-- @author Caroline Åling
--
-- =============================================================================

return function(url, timeout)
    return ("curl --connect-timeout %d -s '%s'"):format(timeout or 3, url)
end
