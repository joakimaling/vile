-- =============================================================================
--
-- Vile helper for Awesome 4.x (~/.config/awesome/vile/helpers/bytes.lua)
--
-- Returns the smallest byte value with corresponding unit.
--
-- @author Caroline Åling
--
-- =============================================================================

return function(value, base)
	base = base or 1024
	local unit = {"K", "M", "G", "T", "P"}
	local index = 0

	while value >= base do
		value = value / base
		index = index + 1
	end

	return ("%d%s"):format(value, unit[index])
end
