-- =============================================================================
--
-- Vile helper for Awesome 4.x (~/.config/awesome/vile/helpers/round.lua)
--
-- Rounds a number to the nearest integer.
--
-- @author Caroline Åling
--
-- =============================================================================

return function(value)
	return math.floor(value + 0.5)
end
