-- =============================================================================
--
-- Vile helper for Awesome 4.x (~/.config/awesome/vile/helpers/hexify.lua)
--
-- Converts a number to hexadecimal.
--
-- @author Caroline Åling
--
-- =============================================================================
local round = require("vils.helpers.round")

return function(value)
	return round((value / 100) * 0x10000)
end,
