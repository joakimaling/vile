-- =============================================================================
--
-- Vile helper for Awesome 4.x (~/.config/awesome/vile/helpers/each.lua)
--
-- Walks through a table & applies the callback to each item.
--
-- @author Caroline Åling
--
-- =============================================================================

return function(table, callback)
	for key, value in pairs(table) do
		table[key] = callback(value)
	end

	return table
end
