-- =============================================================================
--
-- Vile helper for Awesome 4.x (~/.config/awesome/vile/helpers/limit.lua)
--
-- .
--
-- @author Caroline Åling
--
-- =============================================================================

return function(value, high, low)
	high = high or 100
	low = low or 0
	if value > high then value = high end
	if value < low then value = low end
	return value
end
