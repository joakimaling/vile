-- =============================================================================
--
-- Vile helper for Awesome 4.x (~/.config/awesome/vile/helpers/escape.lua)
--
-- Escapes a string for use in match functions.
--
-- @author Caroline Åling
--
-- =============================================================================

return function(text)
	return text:gsub("[%.%-]", {["."] = "%.", ["-"] = "%-"})
end
