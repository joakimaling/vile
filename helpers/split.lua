-- =============================================================================
--
-- Vile helper for Awesome 4.x (~/.config/awesome/vile/helpers/split.lua)
--
-- Splits a string on given delimiter and returns a table.
--
-- @author Caroline Åling
--
-- =============================================================================

return function(text, delimiter)
	delimiter = delimiter or " "
	local output = {}
	for part in text:gmatch(("([^%s]+)"):format(delimiter)) do
		table.insert(output, part)
	end

	return output
end
