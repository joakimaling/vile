-- =============================================================================
--
-- Vile widgets for Awesome 4.x (~/.config/awesome/vile/init.lua)
--
-- @see https://gitlab.com/carolinealing/vile
-- @author Caroline Åling
--
-- =============================================================================
local timer = require("gears.timer")

-- Contains active timers and number of references to each
local timers = {}

-- Updates given widget with data generated by given worker. Widget must have
-- following callback signature; "set_output(self, data)" for this to work.
--
-- @param widget    Vile widget or custom wibox.widget
-- @param worker    Vile worker or custom worker function
-- @param arguments Arguments for the worker
--
local function tick(widget, worker, arguments)
	widget.output = worker(arguments)
end

return {
	-- Creates a complete widget by combining given Vile widget or wibox.widget
	-- with Vile worker or custom worker function. Arguments is a table for the
	-- worker function.
	--
	-- @param widget    Vile widget or custom wibox.widget
	-- @param worker    Vile worker or custom worker function
	-- @param timeout   Interval, in seconds, for the timeout event
	-- @param arguments Arguments for the worker
	--
	construct = function(widget, worker, timeout, arguments)
		arguments = arguments or {}
		timeout = timeout or 2

		-- Construct widgets only if proper timer values are given
		if timeout > 0 then
			local ticker

			-- Increment references if timer with same timeout value exists
			-- Otherwise construct one
			if timers[timeout] then
				ticker = timers[timeout].timer
				timers[timeout].references = 1 + timers[timeout].references
			else
				ticker = timer({timeout = timeout})
				timers[timeout] = {timer = ticker, references = 1}
			end

			-- Connect signal to tick function
			ticker:connect_signal("timeout", function()
				tick(widget, worker, arguments)
			end)

			-- Start timer
			if not ticker.started then
				ticker:start()
			end

			-- Initial run
			tick(widget, worker, arguments)

			-- Return widget object
			return widget
		end

		return nil
	end,

	--
	--
	--
	--
	destruct = function(widget)
		local ticker = timers[widget.timeout]

		ticker.timer:disconnect_signal("timeout", widget.tick)

		ticker.references = ticker.references - 1

		if timers[widget.timeout].references <= 0 then
			ticker.timer:stop()
		end
	end,

	-- Publish widgets
	widgets = require("vile.widgets"),

	-- Publish workers
	workers = require("vile.workers")
}
